from loadSCase import loadSCase
from ShoulderCase.Segmentation.ScapulaSegmentation import ScapulaSegmentation
from ShoulderCase.LandmarkPrediction.LandmarkAndGlenoidSurfacePrediction import LandmarkAndGlenoidSurfacePrediction
from ShoulderCase.Segmentation.SegmentationTraining import SegmentationTraining
from ShoulderCase.LandmarkPrediction.LandmarksTraining import LandmarksTraining
import os
from getConfig import getConfig

def trainDLSegmentationModel(trainSCaseList:list):
    SCases = loadSCase(trainSCaseList)
    scapulaSegmentationDLModel = SegmentationTraining()
    # scapulaSegmentationDLModel.createFolders()
    #
    # for SCase in SCases:
    #     scapulaSegmentationDLModel.getImagesFromScaseID(SCase)
    #     scapulaSegmentationDLModel.getLabelsFromScaseID(SCase)
    scapulaSegmentationDLModel.writeDataset(trainSCaseList)
    scapulaSegmentationDLModel.train()

def trainScapulaLandmarkModel(trainSCaseList:list):
    SCases = loadSCase(trainSCaseList)
    scapulaLandmarkDLModel = LandmarksTraining()
    scapulaLandmarkDLModel.createFolders()
    #
    for SCase in SCases:
        segmentationPaths = scapulaLandmarkDLModel.determineFullbodyOrShoulder(SCase)
        scapulaLandmarkDLModel.getSegmentationsFromScaseIDAndConvertNumpy(SCase, segmentationPaths)
        scapulaLandmarkDLModel.getLabelsFromScaseID(SCase)
        scapulaLandmarkDLModel.dataAugmentationRotation(SCase)

    #scapulaLandmarkDLModel.train()

def segmentSCases(sCaseIDList:list):
    SCases = loadSCase(sCaseIDList)
    for SCase in SCases:
        #if os.path.exists(os.path.join(SCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/segmentations") == True:
        scapulaSegmentationDLModel = ScapulaSegmentation([])
        scapulaSegmentationDLModel.predictScapulaSegmentation(SCase)
        scapulaSegmentationDLModel.saveScapulaSegmentationVTK(SCase)

def predictLandmarksSCases(sCaseIDList:list):
    for sCaseID in sCaseIDList:
        sCase = loadSCase(sCaseID)
        #if os.path.exists(os.path.join(sCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/scapulaSurfaceAutoR.ply") or os.path.exists(os.path.join(sCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/scapulaSurfaceAutoL.ply"):
        landmarkModel = LandmarkAndGlenoidSurfacePrediction([])
        segmentationPaths = landmarkModel.determineFullbodyOrShoulder(sCase)
        landmarks = landmarkModel.predictAndWriteScapulaLandmarks(sCase, segmentationPaths)
        landmarkModel.predictAndWriteGlenoidLandmarks(sCase, segmentationPaths)


