from ShoulderCase.ShoulderCaseLoader.ShoulderCaseLoader import ShoulderCaseLoader 

def createEmptySCase(SCaseID:list):
    """
    Load ShoulderCase objects from the database
    
    Inputs: 
        SCaseID: String array of shoulder case IDs.
        Including "N" in the string array will load all the normal cases.
        Including "P" in the string array will load all the pathological cases.
        Including "*" in the string array will load all the cases.

    Output: 
        Array of the corresponding ShoulderCase objects.
    """
    database = ShoulderCaseLoader()
    if "*" in SCaseID:
        SCaseID = database.getAllCasesID()
    else:
        if "N" in SCaseID:
            SCaseID = SCaseID.remove("N")
            SCaseID = SCaseID.extend(database.getAllNormalCasesID)
            SCaseID = list(set(SCaseID))
        if "P" in SCaseID:
            SCaseID = SCaseID.remove("P")
            SCaseID = SCaseID.extend(database.getAllPathologicalCasesID)
            SCaseID = list(set(SCaseID))
    SCase = database.createEmptyCase(SCaseID)
    return SCase
        
            
            
        
    
    
    
    