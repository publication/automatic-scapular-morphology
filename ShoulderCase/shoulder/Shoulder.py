import os
from ShoulderCase.Humerus.Humerus import Humerus
# from ShoulderCase.RotatorCuff import RotatorCuff
from ShoulderCase.ScapulaAuto.ScapulaAuto import ScapulaAuto
from ShoulderCase.ScapulaManual.ScapulaManual import ScapulaManual
from utils.Logger.Logger import Logger
from utils.recursiveMethodCall import recursiveMethodCall


class Shoulder:
    """
    Contains all the shoulder parts (bones) and their measurements.

    Can be used to plot an overview of the case.
    """

    def __init__(self, SCase, shoulderSide, landmarksAcquisition):
        self.side = shoulderSide
        self.landmarksAcquisition = landmarksAcquisition
        self.SCase = SCase
        if os.path.isdir(self.dataPath()):
            os.mkdir(self.dataPath())

        #self.rotatorCuff = RotatorCuff.RotatorCuff(self)
        if landmarksAcquisition == "auto":
            self.scapula = ScapulaAuto(self)
        elif landmarksAcquisition == "manual":
            self.scapula = ScapulaManual(self)

        self.humerus = Humerus(self)

        self.CTSCan = ""
        self.comment = ""
        self.hasMeasurement = ""

    def hasMeasurements(self):
        self.hasMeasurement = not self.isempty()

    def isempty(self):
        return self.scapula.isempty()

    def explicitSide(self):
        explicitSide = self.side
        explicitSide = explicitSide.replace("R", "right")
        explicitSide = explicitSide.replace("L", "left")
        return explicitSide

    def dataPath(self):
        return os.path.join(self.SCase.dataPythonPath(),
                            "shoulders",
                            self.explicitSide(),
                            self.landmarksAcquisition)

    def loadData(self):
         Logger.newDelimitedSection("Load data")
         Logger.logn("")
         recursiveMethodCall(self, "loadData", [self.SCase])
         Logger.closeSection()
         self.hasMeasurements()

    def morphology(self):
         Logger.newDelimitedSection("Morphology")
         Logger.logn("")
         recursiveMethodCall(self, "morphology", [self.SCase])
         Logger.closeSection()

    def measureFirst(self):
         Logger.newDelimitedSection("First measurements")
         Logger.logn("")
         recursiveMethodCall(self, "measureFirst", [self.SCase])
         Logger.closeSection()

    def measureSecond(self):
         Logger.newDelimitedSection("Second  measurements")
         Logger.logn("")
         recursiveMethodCall(self, "measureSecond", [self.SCase])
         Logger.closeSection()

    def measureThird(self):
         Logger.newDelimitedSection("Third  measurements")
         Logger.logn("")
         recursiveMethodCall(self, "measureThird", [self.SCase])
         Logger.closeSection()

    def measureDensity(self):
        self.scapula.glenoid.measureDensity()

    def measureMuscles(self):
        self.rotatorCuff.sliceAndSegment()
        self.rotatorCuff.measure("rotatorCuffMatthieu", "autoMatthieu")
