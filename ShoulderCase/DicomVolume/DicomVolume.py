import numpy as np
from scipy.ndimage import gaussian_filter
import os
import glob
from readDicomVolume import readSlices, readDicomVolume, readPatientPositions, \
                            readPixelSpacings, readPatientOrientation

class DicomVolume:
    """
    Used to manipulate the data given by dicomreadVolume() and dicominfo().
    Can be used to retrieve a point coordinates given its indices, and vice
    versa.
    """
    def __init__(self):
        self.volume = []
        self.spatial = []
        self.dicomInfo = []
        self.dicomFolderPath = []
    
    def set_volume(self, volume):
        assert len(volume.shape) >=3, "Volume must be a 3D array"
        self.volume = np.squeeze(volume)
        
    def set_spatial(self, spatial):
        requiredDictionaryKeys = np.unique(('PatientPositions',
                                   'PixelSpacings',
                                   'PatientOrientations'))
        assert np.all(np.in1d(np.unique(spatial.keys()),
                              requiredDictionaryKeys)), "Invalid spatial structure"
        self.spatial = spatial
        
    def applyGaussianFilter(self):
        # Apply a gaussian filter to the loaded volume.
        self.volume = gaussian_filter(self.volume, sigma=1)
        
    def assertPathContainsDicomFiles(self, path):
        assert os.path.isdir(path), "Provided argument is not a valid folder path"
        dicomFiles = glob.glob(os.path.join(path), "*.dcm")
        assert dicomFiles, "No dicom file found there %s" % path
    
    def getPointCoordinates(self, pointsIndices):
        # the output are the three coordinates in space of the given volume indices.
        # this function is the inverse of obj.getPointIndexInVolume
        coordinatesX = self.getPointsCoordinatesX(pointsIndices[:, 0])
        coordinatesY = self.getPointsCoordinatesY(pointsIndices[:, 1])
        coordinatesZ = self.getPointsCoordinatesZ(pointsIndices[:, 2])
        return np.concatenate([coordinatesX,
                               coordinatesY,
                               coordinatesZ], axis=1)
    
    def getPointIndexInVolume(self, pointsCoordinates):
        # the output are the three indices of the given points in the given volume.
        indicesX = self.getPointsIndicesX(pointsCoordinates[:, 0])
        indicesY = self.getPointsIndicesY(pointsCoordinates[:, 1])
        indicesZ = self.getPointsIndicesZ(pointsCoordinates[:, 2])
        return np.concatenate([indicesX, indicesY, indicesZ], axis=1)
    
    def getPointsCoordinatesX(self, pointsIndicesX):
        coordinatesX = self.spatial.PatientPositions[0, 0] + \
            (self.spatial.PixelSpacings[0, 0]*pointsIndicesX)
        return coordinatesX

    def getPointsCoordinatesY(self, pointsIndicesY):
        coordinatesY = self.spatial.PatientPositions[0, 1] + \
            (self.spatial.PixelSpacings[0, 1]*pointsIndicesY)
        return coordinatesY        
    
    def getPointsCoordinatesZ(self, pointsIndicesZ):
        pointsLinearIndicesZ = pointsIndicesZ.reshape(-1, 1)
        coordinatesZ = self.spatial.PatientPositions[pointsLinearIndicesZ, 2]
        return coordinatesZ .reshape(pointsIndicesZ.shape)   
    
    def getPointsIndicesX(self, pointsCoordinatesX):
        indicesX = int((pointsCoordinatesX - self.spatial.PatientPositions[0,0])/self.spatial.PixelSpacings[0,0])
        return indicesX
    
    def getPointsIndicesY(self, pointsCoordinatesY):
        indicesY = int((pointsCoordinatesY - self.spatial.PatientPositions[0,1])/self.spatial.PixelSpacings[0,1])
        return indicesY
    
    def getPointsIndicesZ(self, pointsCoordinatesZ):
        pointsLinearCoordinatesZ = pointsCoordinatesZ.reshape(-1, 1)
        patientPositionsZ = self.spatial.PatientPositions[:,3]
        
        differenceFromPatientPositionsToPointsCoordinates = \
            patientPositionsZ - pointsLinearCoordinatesZ.reshape(-1, 1)
        
        pointsLinearIndicesZ = np.argmin(np.abs(differenceFromPatientPositionsToPointsCoordinates),
                  axis=0)
        return pointsLinearIndicesZ.reshape(-1, 1).reshape(pointsCoordinatesZ.shape)
    
    def loadDataFromFolder(self, dicomFolderPath):
        self.assertPathContainsDicomFiles(dicomFolderPath)
        self.dicomFolderPath = dicomFolderPath
        self.volume = readDicomVolume(readSlices(dicomFolderPath))
        self.spatial = {"PatientPositions": \
                        readPatientPositions(readSlices(dicomFolderPath)),
                        "PixelSpacings":\
                        readPixelSpacings(readSlices(dicomFolderPath)),
                        "PatientOrientations":\
                        readPatientOrientation(readSlices(dicomFolderPath))}
        self.loadDicomInfoFromFolder(dicomFolderPath)
    
    def loadDicomInfoFromFolder(self, dicomFolderPath):
        self.assertPathContainsDicomFiles(dicomFolderPath)
        dicomFiles = glob.glob(os.path.join(dicomFolderPath), "*.dcm")
        self.dicomInfo = dicomFiles[0]
    
    def setVolumeToSubvolume(self, center, x, y, z):
        #
        # find boundaries
        volumeSize = self.volume.shape
        minXYZ = self.getPointIndexInVolume(center - np.array([x/2, y/2, z/2]))
        maxXYZ = self.getPointIndexInVolume(center + np.array([x/2, y/2, z/2]))
        left = np.max(1, minXYZ[0])
        right = np.min(volumeSize[0], maxXYZ[0])
        front = np.max(1, minXYZ[1])
        rear = np.min(volumeSize[1], maxXYZ[1])
        bottom = np.max(1, minXYZ[2])
        top = np.min(volumeSize[2], maxXYZ[2])
        
        #set subvolume
        self.volume = self.volume[left:right+1, front:rear+1, bottom:top+1]
        
        #update spatial
        self.spatial.PixelSpacings = self.spatial.PixelSpacings[bottom:top+1, :]
        self.spatial.PatientPositions = self.spatial.PatientPositions[bottom:top+1, :]
        self.spatial.PatientPositions[:, 0] = self.spatial.PatientPositions[:, 0] + \
                                            left*self.spatial.PixelSpacings[:, 0]
        self.spatial.PatientPositions[:, 1] = self.spatial.PatientPositions[:, 1] + \
                                            front*self.spatial.PixelSpacings[:, 1]  
        self.spatial.PatientOrientations = self.spatial.PatientOrientations[:,:,bottom:top+1]                                              