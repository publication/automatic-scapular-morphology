import json
import pydicom as dicom
import os
import numpy as np
import SimpleITK as sitk
import vtk
from scipy.ndimage import zoom
from skimage import measure
import pymeshlab
from getConfig import getConfig
from loadSCase import loadSCase
import math
import random

class LandmarksTraining:
    def __init__(self):
        """
        Please fill this method with all of the necessary initialization for segmenting scapula.
        We instantiate your deep learninf model here.
        You can have the list of the training cases here.
        """
        #self.scapulaSegmentorModel = model
        self.numberOfScapulaLandmarks = 9
        self.angleX = random.uniform(-40, 40)
        self.angleY = random.uniform(-20, 20)
        self.angleZ = random.uniform(-20, 20)

    def Rx(self, theta):
        return np.array([[1, 0, 0, 0],
                         [0, math.cos(theta), -math.sin(theta), 0],
                         [0, math.sin(theta), math.cos(theta), 0],
                         [0, 0, 0, 1]])

    def Ry(self, theta):
        return np.array([[math.cos(theta), 0, math.sin(theta), 0],
                         [0, 1, 0, 0],
                         [-math.sin(theta), 0, math.cos(theta), 0],
                         [0, 0, 0, 1]])

    def Rz(self, theta):
        return np.array([[math.cos(theta), -math.sin(theta), 0, 0],
                         [math.sin(theta), math.cos(theta), 0, 0],
                         [0, 0, 1, 0],
                         [0, 0, 0, 1]])

    def largestConnectedLayer(self, image, background=0):
        labels = measure.label(image, background=background)
        counts = np.bincount(labels.flatten())
        argmax = np.argmax(counts[1:]) + 1

        return (labels == argmax).astype(int)

    def postprocess(self, image):
        single = self.largestConnectedLayer(image)

        return single

    def createFolders(self):
        try:
            os.makedirs(getConfig()["landmarkTrainingParameters"]["segmentedScapulaFolder"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["landmarkTrainingParameters"]["segmentedScapulaNiiFolder"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["landmarkTrainingParameters"]["landmarkGroundTruthFolder"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["landmarkTrainingParameters"]["modelSaveDir"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["landmarkTrainingParameters"]["newParamsFolder"])
        except OSError as error:
            print(error)
    
    def determineFullbodyOrShoulder(self, trainSCase):
        folderPath = os.path.join(trainSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations")
        for file in os.listdir(folderPath):
            if file.endswith(trainSCase.id + ".nii.gz"):
                segmentation_path = os.path.join(folderPath, (trainSCase.id + ".nii.gz"))

                if os.path.isfile(os.path.join(os.path.join(trainSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]), "scapulaSurfaceAutoL.ply")):
                    left_shoulder_path = segmentation_path
                    right_shoulder_path = []

                else:
                    right_shoulder_path = segmentation_path
                    left_shoulder_path = []

            if file.endswith(trainSCase.id + "L.nii.gz"):
                left_shoulder_path = os.path.join(folderPath, (trainSCase.id + "L.nii.gz"))

            if file.endswith(trainSCase.id + "R.nii.gz"):
                right_shoulder_path = os.path.join(folderPath, (trainSCase.id + "R.nii.gz"))

        #print("Returning the paths of all shoulders existing for landmarking.")
        return [left_shoulder_path, right_shoulder_path]


    def getSegmentationsFromScaseIDAndConvertNumpy(self, trainSCase, segmentationPaths):
        landmarks =[]
        #folderPath = os.path.join(trainSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"])
        for j, file in enumerate(segmentationPaths):

            if file == []:
                landmarks.append([])

            else:
                print("Reshaping and resizing the segmentation to be landmarked.")
                segmentation_sitk = sitk.ReadImage(file)
                segmentation = sitk.GetArrayFromImage(segmentation_sitk)
                segmentation = np.moveaxis(segmentation, 0, 2)
                spacing_current = segmentation_sitk.GetSpacing()
                direction = segmentation_sitk.GetDirection()
                origin = segmentation_sitk.GetOrigin()
                origin = list(origin)
                spacing_current = list(spacing_current)
                segmentation[segmentation == 2] = 0
                segmentation = segmentation.astype(np.float32)
                image = segmentation
                image_shape_desired = [128, 128, 128]
                image_shape_current = [image.shape[0], image.shape[1], image.shape[2]]
                spacing_desired = [1.5625, 1.5625, 1.8]

                if j == 0:
                    y_axis_position = np.arange(0, image.shape[1]) * spacing_current[1] + origin[0]
                    origin[0] = np.flip(y_axis_position)[0] * -1

                image_shape_with_desired_spacing = [round(a * b / c) for a, b, c in
                                                    zip(image_shape_desired, spacing_desired, spacing_current)]

                if image_shape_with_desired_spacing[2] > image_shape_current[2]:
                    pad = np.ones(
                        (image.shape[0], image.shape[1],
                         image_shape_with_desired_spacing[2] - image_shape_current[2])) * 0
                    image = np.concatenate((image, pad), axis=2)
                else:

                    diff = image_shape_current[2] - image_shape_with_desired_spacing[2]
                    image = image[:, :, int(np.round(diff / 2)):int(image.shape[2] - np.round(diff / 2))]
                    image = image[:, :, 0:image_shape_with_desired_spacing[2]]
                    origin = [origin[0], origin[1], origin[2] + spacing_current[2] * int(np.round(diff / 2))]

                if image_shape_with_desired_spacing[0] > image_shape_current[0]:
                    borders = np.ones((image_shape_with_desired_spacing[0], image.shape[1],
                                       image.shape[2])) * 0
                    diff_x = int(np.round((image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2))
                    borders[diff_x:diff_x + image_shape_current[0], :, :] = image
                    image = borders
                    new_origin = [origin[0] , origin[1] - diff_x * spacing_current[1], origin[2]]


                else:
                    diff_x = abs(image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2

                    image = image[int(np.ceil(diff_x)):image_shape_current[0] - int(np.floor(diff_x)), :, :]

                    new_origin = [origin[0], origin[1] + int(np.ceil(diff_x)) * spacing_current[1], origin[2]]

                if image_shape_with_desired_spacing[1] > image_shape_current[1]:
                    borders = np.ones((image.shape[0], image_shape_with_desired_spacing[1],
                                       image.shape[2])) * 0
                    diff_y = int(np.round((image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2))
                    borders[:, diff_y:diff_y + image_shape_current[1], :] = image
                    image = borders
                    new_origin = [new_origin[0] - diff_y * spacing_current[0], new_origin[1] ,
                                  new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                else:
                    diff_y = abs(image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2
                    image = image[:, int(np.ceil(diff_y)):image_shape_current[1] - int(np.floor(diff_y)), :]
                    new_origin = [new_origin[0] + int(np.ceil(diff_y)) * spacing_current[0], new_origin[1] , new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                image = zoom(image, (image_shape_desired[0] / image.shape[0], image_shape_desired[1] / image.shape[1],
                                     image_shape_desired[2] / image.shape[2]))

                if j == 0:
                    image = np.flip(image, axis=1)

                image[image >= 0.5] = 1
                image[image < 0.5] = 0

                image = self.postprocess(image)
                image = image - 0.5

                np.save(os.path.join(getConfig()["landmarkTrainingParameters"]["segmentedScapulaFolder"], trainSCase.id + ".npy" ), image)
                np.save(os.path.join(getConfig()["landmarkTrainingParameters"]["newParamsFolder"], trainSCase.id + ".npy"), new_params)
                sitkImage = sitk.GetImageFromArray(np.moveaxis(image,2,0))
                sitkImage.SetSpacing([1.5625, 1.5625, 1.8])
                sitkImage.SetDirection(direction)
                sitkImage.SetOrigin([-98.4375, -98.4375, -115.2])
                sitk.WriteImage(sitkImage, os.path.join(getConfig()["landmarkTrainingParameters"]["segmentedScapulaNiiFolder"], trainSCase.id + ".nii.gz" ))


    def getLabelsFromScaseID(self, trainSCase):
        python_dir = trainSCase.dataPythonPath() + "/"
        groundtruth_dir = python_dir + "groundTruth/landmarks/scapula/"
        jsonfile = groundtruth_dir + trainSCase.id + ".mrk.json"
        f = open(jsonfile,)
        groundTruth_json = json.load(f)
        groundTruth_list = []

        for i in range(self.numberOfScapulaLandmarks):
            groundTruth_list.append(groundTruth_json["markups"][0]["controlPoints"][i]["position"])

        groundTruth = np.array(groundTruth_list).reshape(self.numberOfScapulaLandmarks, 3)

        if groundTruth[0][0] > 0:
            groundTruth[:,0] = -groundTruth[:,0]

        np.save(os.path.join(getConfig()["landmarkTrainingParameters"]["landmarkGroundTruthFolder"], trainSCase.id + ".npy" ), groundTruth)


    #
    def dataAugmentationRotation(self, trainSCase):

        segm = sitk.ReadImage(os.path.join(getConfig()["landmarkTrainingParameters"]["segmentedScapulaNiiFolder"], trainSCase.id + ".nii.gz" ))
        # output_size = segm.GetSize()
        # output_spacing = segm.GetSpacing()
        # output_origin = segm.GetOrigin()
        # output_direction = segm.GetDirection()
        transform = sitk.AffineTransform(3)
        direction = segm.GetDirection()

        for i in range(10):
            # angleX = random.uniform(-40, 40)
            # angleY = random.uniform(-20, 20)
            # angleZ = random.uniform(-20, 20)

            angleX = -21
            angleY = 12
            angleZ = 9

            transform_matrix = self.Rz(angleZ * np.pi / 180) @ self.Ry(
                angleY * np.pi / 180) @ self.Rx(angleX * np.pi / 180)
            param = np.load(
                os.path.join(getConfig()["landmarkTrainingParameters"]["newParamsFolder"], trainSCase.id + ".npy"))

            origin = np.array([param[1][0], param[1][1], param[1][2]])
            new_origin = segm.GetOrigin()
            transform.SetMatrix(transform_matrix[:3, :3].T.flatten())
            transform.SetCenter([0, 0, 0])
            resampled_image = sitk.Resample(segm, segm, transform, sitk.sitkLanczosWindowedSinc, -0.5)
            resampled_image.SetSpacing([1.5625,1.5625,1.8])
            resampled_image.SetOrigin(origin)
            resampled_image.SetDirection(direction)
            sitk.WriteImage(resampled_image, "deneme" +str(i+1)+".nii.gz")

            print(new_origin, origin)
            ##Load, rotate and save landmark
            landmark = np.load(os.path.join(getConfig()["landmarkTrainingParameters"]["landmarkGroundTruthFolder"], trainSCase.id + ".npy" ))
            print(i)

            landmark = landmark + (new_origin - origin)
            print(landmark)

            ##Load, rotate and save landmark
            landmark_rotated = landmark @ self.Rx(-angleX * np.pi / 180)[0:3, 0:3] @ self.Ry(-angleY * np.pi / 180)[0:3, 0:3] @ self.Rz(-angleZ * np.pi / 180)[0:3, 0:3]
            landmark_rotated = landmark_rotated + (origin - new_origin)
            print(landmark_rotated)
            np.save("deneme" + str(i + 1) + ".npy", landmark_rotated)

    # def train(self):


















