
import pickle
import shutil
import pandas as pd
import pydicom as dicom
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import numpy as np
import SimpleITK as sitk
import trimesh
from skimage import measure
import pymeshlab
from getConfig import getConfig
from scipy.ndimage import zoom
import tensorflow as tf
import math

from skspatial.objects import Line, Plane, Vector

class LandmarkAndGlenoidSurfacePrediction:
    def __init__(self, model):

        self.numberOfScapulaLandmarks = 10
        self.numberOfGlenoidLandmarks = 5


        self.scapulaLandmarkPredictorModel = tf.keras.models.load_model(
            getConfig()["scapulaLandmarkModel"],
            custom_objects={"wing_loss": self.wing_loss},
            compile=False)
        self.glenoidLandmarkPredictorModel = tf.keras.models.load_model(
            getConfig()["glenoidLandmarkModel"],
            custom_objects={"wing_loss": self.wing_loss},
            compile=False)

        self.meanCurvatureThreshold = 0.02

    def wing_loss(self, landmarks, labels, w=3.0, epsilon=0.5):
        """
        Arguments:
            landmarks, labels: float tensors with shape [batch_size, num_landmarks, 2].
            w, epsilon: a float numbers.
        Returns:
            a float tensor with shape [].
        """
        with tf.name_scope('wing_loss'):
            x = landmarks - labels
            c = w * (1.0 - math.log(1.0 + w / epsilon))
            absolute_x = tf.abs(x)
            losses = tf.where(tf.greater(w, absolute_x), w * tf.math.log(1.0 + absolute_x / epsilon), absolute_x - c)

            loss = tf.reduce_mean(losses, axis=1)

            return loss

    def largestConnectedLayer(self, image, background=0):
        labels = measure.label(image, background=background)
        counts = np.bincount(labels.flatten())
        argmax = np.argmax(counts[1:]) + 1
        return (labels == argmax).astype(int)

    def postprocess(self, image):
        single = self.largestConnectedLayer(image)

        return single

    def train(self, trainingSCases):
        """
        Here we train the deep learning model on training subjects
        :param trainingData:
        :return:
        """
        self.scapulaSegmentorModel.train(trainingSCases)

    def determineFullbodyOrShoulder(self, testSCase):
        folderPath = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations")
        for file in os.listdir(folderPath):
            if file.endswith(testSCase.id + ".nii.gz"):
                segmentation_path = os.path.join(folderPath, (testSCase.id + ".nii.gz"))

                if os.path.isfile(os.path.join(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]), "scapulaSurfaceAutoL.ply")):
                    left_shoulder_path = segmentation_path
                    right_shoulder_path = []

                else:
                    right_shoulder_path = segmentation_path
                    left_shoulder_path = []

            if file.endswith(testSCase.id + "L.nii.gz"):
                left_shoulder_path = os.path.join(folderPath, (testSCase.id + "L.nii.gz"))

            if file.endswith(testSCase.id + "R.nii.gz"):
                right_shoulder_path = os.path.join(folderPath, (testSCase.id + "R.nii.gz"))

        #print("Returning the paths of all shoulders existing for landmarking.")
        return [left_shoulder_path, right_shoulder_path]

    def predictAndWriteScapulaLandmarks(self, testSCase, segmentationPaths):
        landmarks =[]
        folderPath = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"])
        for j, file in enumerate(segmentationPaths):

            if file == []:
                landmarks.append([])

            else:
                print("Reshaping and resizing the segmentation to be landmarked.")
                segmentation_sitk = sitk.ReadImage(file)
                segmentation = sitk.GetArrayFromImage(segmentation_sitk)
                segmentation = np.moveaxis(segmentation, 0, 2)
                spacing_current = segmentation_sitk.GetSpacing()
                direction = segmentation_sitk.GetDirection()
                origin = segmentation_sitk.GetOrigin()
                origin = list(origin)
                spacing_current = list(spacing_current)
                segmentation[segmentation == 2] = 0
                segmentation = segmentation.astype(np.float32)
                image = segmentation
                image_shape_desired = [128, 128, 128]
                image_shape_current = [image.shape[0], image.shape[1], image.shape[2]]
                spacing_desired = [1.5625, 1.5625, 1.8]

                if j == 0:
                    y_axis_position = np.arange(0, image.shape[1]) * spacing_current[1] + origin[0]
                    origin[0] = np.flip(y_axis_position)[0] * -1

                image_shape_with_desired_spacing = [round(a * b / c) for a, b, c in
                                                    zip(image_shape_desired, spacing_desired, spacing_current)]

                if image_shape_with_desired_spacing[2] > image_shape_current[2]:
                    pad = np.ones(
                        (image.shape[0], image.shape[1],
                         image_shape_with_desired_spacing[2] - image_shape_current[2])) * 0
                    image = np.concatenate((image, pad), axis=2)
                else:

                    diff = image_shape_current[2] - image_shape_with_desired_spacing[2]
                    image = image[:, :, int(np.round(diff / 2)):int(image.shape[2] - np.round(diff / 2))]
                    image = image[:, :, 0:image_shape_with_desired_spacing[2]]
                    origin = [origin[0], origin[1], origin[2] + spacing_current[2] * int(np.round(diff / 2))]

                if image_shape_with_desired_spacing[0] > image_shape_current[0]:
                    borders = np.ones((image_shape_with_desired_spacing[0], image.shape[1],
                                       image.shape[2])) * 0
                    diff_x = int(np.round((image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2))
                    borders[diff_x:diff_x + image_shape_current[0], :, :] = image
                    image = borders
                    new_origin = [origin[0] , origin[1] - diff_x * spacing_current[1], origin[2]]


                else:
                    diff_x = abs(image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2

                    image = image[int(np.ceil(diff_x)):image_shape_current[0] - int(np.floor(diff_x)), :, :]

                    new_origin = [origin[0], origin[1] + int(np.ceil(diff_x)) * spacing_current[1], origin[2]]



                if image_shape_with_desired_spacing[1] > image_shape_current[1]:
                    borders = np.ones((image.shape[0], image_shape_with_desired_spacing[1],
                                       image.shape[2])) * 0
                    diff_y = int(np.round((image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2))
                    borders[:, diff_y:diff_y + image_shape_current[1], :] = image
                    image = borders
                    new_origin = [new_origin[0] - diff_y * spacing_current[0], new_origin[1] ,
                                  new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                else:
                    diff_y = abs(image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2
                    image = image[:, int(np.ceil(diff_y)):image_shape_current[1] - int(np.floor(diff_y)), :]
                    new_origin = [new_origin[0] + int(np.ceil(diff_y)) * spacing_current[0], new_origin[1] , new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                image = zoom(image, (image_shape_desired[0] / image.shape[0], image_shape_desired[1] / image.shape[1],
                                     image_shape_desired[2] / image.shape[2]))



                if j == 0:
                    image = np.flip(image, axis=1)


                image[image >= 0.5] = 1
                image[image < 0.5] = 0
                # image=image+0.5

                image = self.postprocess(image)
                image = image - 0.5

                spacing = new_params[0]
                origin = new_params[1]
                y_axis_position = np.arange(0, 128) * spacing[0] + origin[0]
                x_axis_position = np.arange(0, 128) * spacing[1] + origin[1]
                z_axis_position = np.arange(0, 128) * spacing[2] + origin[2]
                ygrid, xgrid, zgrid = np.meshgrid(y_axis_position, x_axis_position, z_axis_position)
                array_for_coordinates = np.stack((ygrid, xgrid, zgrid), axis=-1)
                sitkImage = sitk.GetImageFromArray(image)

                image = image.reshape(1, 128, 128, 128, 1)
                print("Landmark prediction begins.")
                prediction = self.scapulaLandmarkPredictorModel.predict(image)
                val_logits_total = []
                for i in range(self.numberOfScapulaLandmarks):
                    val_logits_total.append(tf.reshape(tf.reduce_sum(
                        tf.reshape(prediction[0, :, :, :, i], [128, 128, 128, 1]) * (array_for_coordinates),
                        axis=[0, 1, 2]), [1, 3]))
                landmarks_predicted = np.array(val_logits_total)

                if j == 0:
                    landmarks_predicted[:, :, 0] = -landmarks_predicted[:, :, 0]

                landmarks.append(landmarks_predicted)
                dict_landmarks = {}
                dict_keys = ['angulusInferior', 'trigonumSpinae', 'processusCoracoideus', 'acromioClavicular',
                             'angulusAcromialis', 'spinoGlenoidNotch']

                for key_nr in range(len(dict_keys)):
                    dict_landmarks[dict_keys[key_nr]] = landmarks_predicted[key_nr]

                dict_landmarks['groove'] = landmarks_predicted[6:9].reshape(3, 3)

                if j == 0:
                    with open(os.path.join(folderPath, "scapulaLandmarksAutoL.pkl"),
                              'wb') as handle:
                        pickle.dump(dict_landmarks, handle)

                if j == 1:
                    with open(os.path.join(folderPath, "scapulaLandmarksAutoR.pkl"),
                              'wb') as handle:
                        pickle.dump(dict_landmarks, handle)
                print("Landmarks have been saved")
        return landmarks

    def predictAndWriteHumerusLandmarks(self, testSCase, segmentationPaths):
        landmarks = []
        folderPath = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"])
        for j, file in enumerate(segmentationPaths):

            if file == []:
                landmarks.append([])

            else:
                print("Reshaping and resizing the segmentation to be landmarked.")
                segmentation_sitk = sitk.ReadImage(file)
                segmentation = sitk.GetArrayFromImage(segmentation_sitk)
                #segmentation = np.moveaxis(segmentation, 0, 2)
                spacing_current = segmentation_sitk.GetSpacing()
                origin = segmentation_sitk.GetOrigin()
                origin = list(origin)
                spacing_current = list(spacing_current)
                segmentation[segmentation == 1] = 0
                segmentation[segmentation == 2] = 1
                humerus = segmentation.astype(np.uint8)
                humerus_sitk = sitk.GetImageFromArray(humerus)
                humerus_sitk.SetSpacing(segmentation_sitk.GetSpacing())
                humerus_sitk.SetOrigin(segmentation_sitk.GetOrigin())
                humerus_sitk.SetDirection(segmentation_sitk.GetDirection())
                bb_filter = sitk.LabelShapeStatisticsImageFilter()
                bb_filter.Execute(humerus_sitk)
                bb=bb_filter.GetBoundingBox(1)

                cropped_humerus = humerus_sitk[bb[0]:bb[0]+bb[3],bb[1]:bb[1]+bb[4], bb[2]:bb[2]+bb[5]]
                cropped_humerus_np = sitk.GetArrayFromImage(cropped_humerus)
                spacing_current = cropped_humerus.GetSpacing()
                origin = cropped_humerus.GetOrigin()
                spacing_current = list(spacing_current)
                origin = list(origin)

                image = np.moveaxis(cropped_humerus_np, 0, 2)
                image_shape_desired = [128, 128, 128]
                image_shape_current = [image.shape[0], image.shape[1], image.shape[2]]
                spacing_desired = [1, 1, 1]

                if j == 0:
                    y_axis_position = np.arange(0, image.shape[1]) * spacing_current[1] + origin[0]
                    origin[0] = np.flip(y_axis_position)[0] * -1

                image_shape_with_desired_spacing = [round(a * b / c) for a, b, c in
                                                    zip(image_shape_desired, spacing_desired, spacing_current)]

                if image_shape_with_desired_spacing[2] > image_shape_current[2]:
                    pad = np.ones(
                        (image.shape[0], image.shape[1],
                         image_shape_with_desired_spacing[2] - image_shape_current[2])) * 0
                    image = np.concatenate((image, pad), axis=2)
                else:

                    diff = image_shape_current[2] - image_shape_with_desired_spacing[2]
                    image = image[:, :, int(np.round(diff)):image.shape[2]]
                    #image = image[:, :, 0:image_shape_with_desired_spacing[2]]
                    origin = [origin[0], origin[1], origin[2] + spacing_current[2] * int(np.round(diff))]

                if image_shape_with_desired_spacing[0] > image_shape_current[0]:
                    borders = np.ones((image_shape_with_desired_spacing[0], image.shape[1],
                                      image.shape[2])) * 0
                    diff_x = int(np.round((image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2))
                    borders[diff_x:diff_x + image_shape_current[0], :, :] = image
                    image = borders
                    new_origin = [origin[0], origin[1] - diff_x * spacing_current[1], origin[2]]


                else:
                    diff_x = abs(image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2

                    image = image[int(np.ceil(diff_x)):image_shape_current[0] - int(np.floor(diff_x)), :, :]

                    new_origin = [origin[0], origin[1] + int(np.ceil(diff_x)) * spacing_current[1], origin[2]]

                if image_shape_with_desired_spacing[1] > image_shape_current[1]:
                    borders = np.ones((image.shape[0], image_shape_with_desired_spacing[1],
                                       image.shape[2])) * 0
                    diff_y = int(np.round((image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2))
                    borders[:, diff_y:diff_y + image_shape_current[1], :] = image
                    image = borders
                    new_origin = [new_origin[0] - diff_y * spacing_current[0], new_origin[1],
                                  new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                else:
                    diff_y = abs(image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2
                    image = image[:, int(np.ceil(diff_y)):image_shape_current[1] - int(np.floor(diff_y)), :]
                    new_origin = [new_origin[0] + int(np.ceil(diff_y)) * spacing_current[0], new_origin[1],
                                  new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                image = zoom(image, (image_shape_desired[0] / image.shape[0], image_shape_desired[1] / image.shape[1],
                                     image_shape_desired[2] / image.shape[2]))


                if j == 0:
                    image = np.flip(image, axis=1)

                image[image >= 0.5] = 1
                image[image < 0.5] = 0

                # image=image+0.5

                image = self.postprocess(image)
                image = image - 0.5
                spacing = new_params[0]
                origin = new_params[1]
                y_axis_position = np.arange(0, 128) * spacing[0] + origin[0]
                x_axis_position = np.arange(0, 128) * spacing[1] + origin[1]
                z_axis_position = np.arange(0, 128) * spacing[2] + origin[2]
                ygrid, xgrid, zgrid = np.meshgrid(y_axis_position, x_axis_position, z_axis_position)
                array_for_coordinates = np.stack((ygrid, xgrid, zgrid), axis=-1)
                image = image.reshape(1, 128, 128, 128, 1)

                print("Landmark prediction begins.")
                prediction = self.humerusLandmarkPredictorModel.predict(image)
                val_logits_total = []
                for i in range(self.numberOfHumerusLandmarks):
                    val_logits_total.append(tf.reshape(tf.reduce_sum(
                        tf.reshape(prediction[0, :, :, :, i], [128, 128, 128, 1]) * (array_for_coordinates),
                        axis=[0, 1, 2]), [1, 3]))
                landmarks_predicted = np.array(val_logits_total)

                if j == 0:
                    landmarks_predicted[:, :, 0] = -landmarks_predicted[:, :, 0]

                landmarks.append(landmarks_predicted)
                dict_landmarks = {}
                dict_keys = ['intratubercular_groove', 'infraspinatus', 'articular_top', 'articular_bottom',
                             'articular_middle']

                for key_nr in range(len(dict_keys)):
                    dict_landmarks[dict_keys[key_nr]] = landmarks_predicted[key_nr]


                if j == 0:
                    with open(os.path.join(folderPath, "humerusLandmarksAutoL.pkl"),
                              'wb') as handle:
                        pickle.dump(dict_landmarks, handle)

                if j == 1:
                    with open(os.path.join(folderPath, "humerusLandmarksAutoR.pkl"),
                              'wb') as handle:
                        pickle.dump(dict_landmarks, handle)
                print("Landmarks have been saved")


    def extractGlenoidSurface(self, testSCase, landmarks):
        print("Glenoid surface extraction begins.")
        pathFolder = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"])
        for file in os.listdir(pathFolder):
            if file.startswith("scapula") and file.endswith("L.ply"): #First 2 if statements to determine if we use left or right shoulder landmarks.
                seed_point = np.squeeze(landmarks[0])[-1]

            if file.startswith("scapula") and file.endswith("R.ply"):
                seed_point = np.squeeze(landmarks[1])[-1]

            if file.startswith("scapula") and file.endswith(".ply"):


                ms = pymeshlab.MeshSet()
                ms.load_new_mesh(os.path.join(pathFolder, file))

                verts_original = ms.current_mesh().vertex_matrix()
                faces_original = ms.current_mesh().face_matrix()
                ms.apply_coord_taubin_smoothing(lambda_=0.5, mu=0.53)
                verts_smoothed = ms.current_mesh().vertex_matrix()
                faces_smoothed = ms.current_mesh().face_matrix()
                normal_verts_smoothed = ms.current_mesh().vertex_normal_matrix()

                distances = np.empty((len(verts_smoothed)))

                for i, element in enumerate(verts_smoothed):
                    distances[i] = np.linalg.norm(element - seed_point)
                seed_vert = np.argmin(distances)
                seed_point_on_the_mesh = verts_smoothed[np.argmin(distances)]

                #Calculate the Geodesic Distance from the seed vertice and the normal of the seed vertice
                normalSeedPoint = normal_verts_smoothed[seed_vert] / np.linalg.norm(normal_verts_smoothed[seed_vert])
                geo_dist_val = 20
                m2 = pymeshlab.Mesh(verts_smoothed, faces_smoothed, f_scalar_array=np.zeros(len(faces_smoothed)))
                msGeodesicDistance = pymeshlab.MeshSet()
                msGeodesicDistance.add_mesh(m2)
                msGeodesicDistance.compute_scalar_by_geodesic_distance_from_given_point_per_vertex(startpoint=seed_point_on_the_mesh)
                msGeodesicDistance.compute_selection_by_condition_per_vertex(condselect="q>{}".format(geo_dist_val))
                msGeodesicDistance.meshing_remove_selected_vertices()
                msGeodesicDistance.meshing_remove_unreferenced_vertices()
                msGeodesicDistance.meshing_repair_non_manifold_vertices()
                msGeodesicDistance.meshing_remove_connected_component_by_diameter()

                verts_geodesicDistance = msGeodesicDistance.current_mesh().vertex_matrix()
                faces_geodesicDistance = msGeodesicDistance.current_mesh().face_matrix()

                mGeodesicDistance = pymeshlab.Mesh(verts_geodesicDistance, faces_geodesicDistance)
                msGeodesicDistance = pymeshlab.MeshSet()
                msGeodesicDistance.add_mesh(mGeodesicDistance)


                anglesBetweenSeedPointAndGeodesicDistanceSelection = []
                for index, points in enumerate(msGeodesicDistance.current_mesh().vertex_matrix()):
                    normalGeodesicDistanceSelection = msGeodesicDistance.current_mesh().vertex_normal_matrix()[index] / np.linalg.norm(
                        msGeodesicDistance.current_mesh().vertex_normal_matrix()[index])
                    anglesBetweenSeedPointAndGeodesicDistanceSelection.append(np.degrees(np.arccos(np.dot(normalSeedPoint, normalGeodesicDistanceSelection))))
                
                
                ##Eliminate the points if the angle between the normal of seed point and the normal of the point is bigger than 30 degrees (only geodesically selected points)
                ## This geodesic distance + normal angle elimination gives us some points, which we fit a sphere to.
                mFitSphere = pymeshlab.Mesh(verts_geodesicDistance, faces_geodesicDistance, v_scalar_array=anglesBetweenSeedPointAndGeodesicDistanceSelection)
                msFitSphere = pymeshlab.MeshSet()
                msFitSphere.add_mesh(mFitSphere)
                msFitSphere.compute_selection_by_condition_per_vertex(condselect="q>30")
                msFitSphere.meshing_remove_selected_vertices()
                msFitSphere.meshing_remove_unreferenced_vertices()
                msFitSphere.meshing_repair_non_manifold_vertices()
                msFitSphere.meshing_remove_connected_component_by_diameter()
                fit_sphere_points = msFitSphere.current_mesh().vertex_matrix()
                center, radius, _ = trimesh.nsphere.fit_nsphere(fit_sphere_points, prior=None)

                verts_distanceBased = []
                for ind, ps in enumerate(verts_smoothed):
                    distance = np.linalg.norm(ps - center) - radius
                    if (np.linalg.norm(ps - seed_point) < 25) and (distance < 2):
                        verts_distanceBased.append(ind)

                anglesBetweenEachVertexAndTheLineToTheSphere = []
                verts_angleBased = []
                for ind, vert in enumerate(verts_smoothed):
                    v1 = normal_verts_smoothed[ind] / np.linalg.norm(normal_verts_smoothed[ind])
                    line_verts = Line.from_points(vert, center)
                    angle = line_verts.vector.angle_between(v1)
                    angle_degrees = np.degrees(angle)
                    anglesBetweenEachVertexAndTheLineToTheSphere.append(angle_degrees)

                    if abs(angle_degrees) < 60:
                        verts_angleBased.append(ind)

                verts_overlap = np.intersect1d(verts_distanceBased, verts_angleBased)
                verts_values = np.zeros((len(verts_smoothed)))
                verts_values[verts_overlap] = 1000

                m = pymeshlab.Mesh(verts_smoothed, faces_smoothed, v_scalar_array=verts_values)
                ms_initialSelection = pymeshlab.MeshSet()
                ms_initialSelection.add_mesh(m)
                ms_initialSelection.compute_selection_by_condition_per_vertex(condselect="q<500")
                ms_initialSelection.meshing_remove_selected_vertices()
                ms_initialSelection.meshing_remove_unreferenced_vertices()
                ms_initialSelection.meshing_repair_non_manifold_vertices()
                ms_initialSelection.meshing_remove_connected_component_by_face_number()


                ms = pymeshlab.MeshSet()
                ms.add_mesh(pymeshlab.Mesh(verts_smoothed, faces_smoothed, v_scalar_array=verts_values))
                #
                #
                vertex_quality = ms.current_mesh().vertex_scalar_array()
                initialSelection = np.where(vertex_quality == 1000)

                ms.compute_curvature_principal_directions_per_vertex(method=0, curvcolormethod=2)
                min_curvature = ms.current_mesh().vertex_scalar_array()
                ms.compute_curvature_principal_directions_per_vertex(method=0, curvcolormethod=3)
                max_curvature = ms.current_mesh().vertex_scalar_array()
                d1 = ms.current_mesh().vertex_curvature_principal_dir1_matrix()
                d2 = ms.current_mesh().vertex_curvature_principal_dir2_matrix()
                ms.compute_curvature_principal_directions_per_vertex(method=0, curvcolormethod=0)
                directionalCurvatures = np.ones((len(verts_smoothed)))

                for i in initialSelection[0]:
                    if i != seed_vert:
                        plane = Plane.from_vectors(verts_smoothed[i], d1[i], d2[i])
                        vector_to_be_projected = Vector.from_points(verts_smoothed[seed_vert], verts_smoothed[i]) / np.linalg.norm(
                            Vector.from_points(verts_smoothed[seed_vert], verts_smoothed[i]))
                        projected_vector = plane.project_vector(
                            [vector_to_be_projected[0], vector_to_be_projected[1], vector_to_be_projected[2]])
                        angle = Vector(d2[i]).angle_between(projected_vector)
                        curvature1 = max_curvature[i] * (np.cos(angle) * np.cos(angle)) + min_curvature[i] * (
                                np.sin(angle) * np.sin(angle))
                        directionalCurvatures[i] = curvature1

                value = np.ones((len(vertex_quality), 1))
                value[directionalCurvatures < self.meanCurvatureThreshold] = 1000
                m = pymeshlab.Mesh(verts_original, faces_original, v_scalar_array=value)
                ms2 = pymeshlab.MeshSet()
                ms2.add_mesh(m)
                ms2.compute_color_from_scalar_per_vertex(minval=1, maxval=1000)
                ms2.compute_selection_by_condition_per_vertex(condselect="q<999")
                ms2.meshing_remove_selected_vertices()
                ms2.meshing_remove_unreferenced_vertices()
                ms2.meshing_repair_non_manifold_vertices()

                ms2.generate_alpha_shape(alpha=pymeshlab.Percentage(40), filtering=1)
                ms2.add_mesh(ms_initialSelection.current_mesh())
                ms2.set_current_mesh(2)
                ms2.compute_scalar_by_distance_from_another_mesh_per_vertex(measuremesh=2, refmesh=1, signeddist=False)
                ms2.compute_selection_by_condition_per_vertex(condselect="q>0.5")
                ms2.meshing_remove_selected_vertices()
                ms2.meshing_remove_unreferenced_vertices()
                ms2.meshing_repair_non_manifold_vertices()
                ms2.save_current_mesh(os.path.join(pathFolder, file.replace("scapula", "glenoid")))

                print("Glenoid surface is extracted.")

    def predictAndWriteGlenoidLandmarks(self, testSCase, segmentationPaths):
        landmarks =[]
        folderPath = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"])
        for j, file in enumerate(segmentationPaths):

            if file == []:
                landmarks.append([])

            else:
                #print("Reshaping and resizing the segmentation to be landmarked.")
                segmentation_sitk = sitk.ReadImage(file)
                segmentation = sitk.GetArrayFromImage(segmentation_sitk)
                segmentation = np.moveaxis(segmentation, 0, 2)
                spacing_current = segmentation_sitk.GetSpacing()
                origin = segmentation_sitk.GetOrigin()
                origin = list(origin)
                spacing_current = list(spacing_current)
                segmentation[segmentation == 2] = 0
                segmentation = segmentation.astype(np.float32)
                image = segmentation
                image_shape_desired = [128, 128, 128]
                image_shape_current = [image.shape[0], image.shape[1], image.shape[2]]
                spacing_desired = [1.5625, 1.5625, 1.8]

                if j == 0:
                    y_axis_position = np.arange(0, image.shape[1]) * spacing_current[1] + origin[0]
                    origin[0] = np.flip(y_axis_position)[0] * -1

                image_shape_with_desired_spacing = [round(a * b / c) for a, b, c in
                                                    zip(image_shape_desired, spacing_desired, spacing_current)]

                if image_shape_with_desired_spacing[2] > image_shape_current[2]:
                    pad = np.ones(
                        (image.shape[0], image.shape[1],
                         image_shape_with_desired_spacing[2] - image_shape_current[2])) * 0
                    image = np.concatenate((image, pad), axis=2)
                else:

                    diff = image_shape_current[2] - image_shape_with_desired_spacing[2]
                    image = image[:, :, int(np.round(diff / 2)):int(image.shape[2] - np.round(diff / 2))]
                    image = image[:, :, 0:image_shape_with_desired_spacing[2]]
                    origin = [origin[0], origin[1], origin[2] + spacing_current[2] * int(np.round(diff / 2))]

                if image_shape_with_desired_spacing[0] > image_shape_current[0]:
                    borders = np.ones((image_shape_with_desired_spacing[0], image.shape[1],
                                       image.shape[2])) * 0
                    diff_x = int(np.round((image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2))
                    borders[diff_x:diff_x + image_shape_current[0], :, :] = image
                    image = borders
                    new_origin = [origin[0] , origin[1] - diff_x * spacing_current[1], origin[2]]


                else:
                    diff_x = abs(image_shape_with_desired_spacing[0] - image_shape_current[0]) / 2

                    image = image[int(np.ceil(diff_x)):image_shape_current[0] - int(np.floor(diff_x)), :, :]

                    new_origin = [origin[0], origin[1] + int(np.ceil(diff_x)) * spacing_current[1], origin[2]]



                if image_shape_with_desired_spacing[1] > image_shape_current[1]:
                    borders = np.ones((image.shape[0], image_shape_with_desired_spacing[1],
                                       image.shape[2])) * 0
                    diff_y = int(np.round((image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2))
                    borders[:, diff_y:diff_y + image_shape_current[1], :] = image
                    image = borders
                    new_origin = [new_origin[0] - diff_y * spacing_current[0], new_origin[1] ,
                                  new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                else:
                    diff_y = abs(image_shape_with_desired_spacing[1] - image_shape_current[1]) / 2
                    image = image[:, int(np.ceil(diff_y)):image_shape_current[1] - int(np.floor(diff_y)), :]
                    new_origin = [new_origin[0] + int(np.ceil(diff_y)) * spacing_current[0], new_origin[1] , new_origin[2]]
                    new_params = [spacing_desired, new_origin]

                image = zoom(image, (image_shape_desired[0] / image.shape[0], image_shape_desired[1] / image.shape[1],
                                     image_shape_desired[2] / image.shape[2]))



                if j == 0:
                    image = np.flip(image, axis=1)


                image[image >= 0.5] = 1
                image[image < 0.5] = 0
                # image=image+0.5

                image = self.postprocess(image)
                image = image - 0.5

                spacing = new_params[0]
                origin = new_params[1]
                y_axis_position = np.arange(0, 128) * spacing[0] + origin[0]
                x_axis_position = np.arange(0, 128) * spacing[1] + origin[1]
                z_axis_position = np.arange(0, 128) * spacing[2] + origin[2]
                ygrid, xgrid, zgrid = np.meshgrid(y_axis_position, x_axis_position, z_axis_position)
                array_for_coordinates = np.stack((ygrid, xgrid, zgrid), axis=-1)
                image = image.reshape(1, 128, 128, 128, 1)
                print("Glenoid landmark prediction begins.")
                prediction = self.glenoidLandmarkPredictorModel.predict(image)
                val_logits_total = []
                for i in range(self.numberOfGlenoidLandmarks):
                    val_logits_total.append(tf.reshape(tf.reduce_sum(
                        tf.reshape(prediction[0, :, :, :, i], [128, 128, 128, 1]) * (array_for_coordinates),
                        axis=[0, 1, 2]), [1, 3]))
                landmarks_predicted = np.array(val_logits_total)

                if j == 0:
                    landmarks_predicted[:, :, 0] = -landmarks_predicted[:, :, 0]

                landmarks.append(landmarks_predicted)
                dict_landmarks = {}
                dict_keys = ['glenoid1', 'glenoid2', 'glenoid3', 'glenoid4',
                             'glenoid5']

                for key_nr in range(len(dict_keys)):
                    dict_landmarks[dict_keys[key_nr]] = landmarks_predicted[key_nr]

                if j == 0:
                    with open(os.path.join(folderPath, "glenoidLandmarksAutoL.pkl"),
                              'wb') as handle:
                        pickle.dump(dict_landmarks, handle)

                    self.extractGlenoidSurfaceWithLandmarks(os.path.join(folderPath, "scapulaSurfaceAutoL.ply"), os.path.join(folderPath, "glenoidLandmarksAutoL.pkl"), os.path.join(folderPath, "glenoidSurfaceAutoL.ply"))

                if j == 1:
                    with open(os.path.join(folderPath, "glenoidLandmarksAutoR.pkl"),
                              'wb') as handle:
                        pickle.dump(dict_landmarks, handle)

                    self.extractGlenoidSurfaceWithLandmarks(os.path.join(folderPath, "scapulaSurfaceAutoR.ply"), os.path.join(folderPath, "glenoidLandmarksAutoR.pkl"), os.path.join(folderPath, "glenoidSurfaceAutoR.ply"))

                print("Glenoid landmarks have been saved")

    def extractGlenoidSurfaceWithLandmarks(self, meshPath, landmarkPath, savePath):

        perc = pymeshlab.Percentage(10)
        ms = pymeshlab.MeshSet()
        ms.load_new_mesh(meshPath)
        verts = ms.current_mesh().vertex_matrix()
        faces = ms.current_mesh().face_matrix()
        indices = []
        with open(landmarkPath, 'rb') as f:
            landmarks = pickle.load(f)

        val_logits_total = [landmarks["glenoid1"], landmarks["glenoid2"], landmarks["glenoid3"],landmarks["glenoid4"],landmarks["glenoid5"]]

        for point in val_logits_total:
            distances = []
            for a in ms.current_mesh().vertex_matrix():
                distances.append(np.linalg.norm(a - point))
            indices.append(np.argmin(distances))
        vertices_to_select = []


        ms.compute_scalar_by_geodesic_distance_from_given_point_per_vertex(startpoint=val_logits_total[0][0],
                                                                           maxdistance=perc)
        array_to_cut = [ms.current_mesh().vertex_scalar_array()[indices[1]],
                        ms.current_mesh().vertex_scalar_array()[indices[1]]]
        ms.compute_scalar_by_geodesic_distance_from_given_point_per_vertex(startpoint=val_logits_total[2][0],
                                                                           maxdistance=perc)
        array_to_cut.append(ms.current_mesh().vertex_scalar_array()[indices[3]])
        array_to_cut.append(ms.current_mesh().vertex_scalar_array()[indices[3]])

        for i in range(4):
            ms.compute_scalar_by_geodesic_distance_from_given_point_per_vertex(startpoint=val_logits_total[i][0],
                                                                               maxdistance=perc)
            vertices_to_select.append(
                np.intersect1d(np.where(ms.current_mesh().vertex_scalar_array() < array_to_cut[i]),
                               np.where(ms.current_mesh().vertex_scalar_array() != 0)))

        intersection1 = np.intersect1d(vertices_to_select[0], vertices_to_select[1])
        intersection2 = np.intersect1d(intersection1, vertices_to_select[2])
        intersection_final = np.intersect1d(intersection2, vertices_to_select[3])
        vertex_quality_array = np.zeros(len(ms.current_mesh().vertex_matrix()))
        vertex_quality_array[intersection_final] = 1

        ms = pymeshlab.MeshSet()
        m = pymeshlab.Mesh(verts, faces, v_scalar_array=vertex_quality_array)
        ms.add_mesh(m)
        ms.compute_selection_by_condition_per_vertex(condselect="q<0.5")
        ms.meshing_remove_selected_vertices()
        ms.meshing_remove_unreferenced_vertices()
        ms.meshing_repair_non_manifold_vertices()
        ms.meshing_remove_connected_component_by_diameter()

        distances2 = []
        for b in ms.current_mesh().vertex_matrix():
            distances2.append(np.linalg.norm(b - val_logits_total[4][0]))

        verts = ms.current_mesh().vertex_matrix()
        faces = ms.current_mesh().face_matrix()

        ms.compute_curvature_principal_directions_per_vertex(method=3, curvcolormethod=0)
        curvatures = ms.current_mesh().vertex_scalar_array()
        eliminated_curvature = np.where(curvatures > self.meanCurvatureThreshold)
        ms.compute_scalar_by_border_distance_per_vertex()
        border_distances = ms.current_mesh().vertex_scalar_array()
        eliminated_border_distances = np.where(border_distances < 2)
        intersection_final = np.intersect1d(eliminated_curvature, eliminated_border_distances)
        #
        vertex_new_array = np.zeros(len(verts))
        vertex_new_array[intersection_final] = 1

        ms = pymeshlab.MeshSet()
        m = pymeshlab.Mesh(verts, faces, v_scalar_array=vertex_new_array)
        ms.add_mesh(m)
        #ms.compute_color_from_scalar_per_vertex()
        ms.compute_selection_by_condition_per_vertex(condselect="q>0.5")
        ms.meshing_remove_selected_vertices()
        ms.meshing_remove_unreferenced_vertices()
        ms.meshing_repair_non_manifold_vertices()
        ms.meshing_remove_connected_component_by_diameter()
        ms.save_current_mesh(savePath)





    def cutCheck(self, testSCase, segmentationPaths):
        landmarks = []
        folderPath = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"])
        for j, file in enumerate(segmentationPaths):

            if file == []:
                landmarks.append([])

            else:
                # print("Reshaping and resizing the segmentation to be landmarked.")
                segmentation_sitk = sitk.ReadImage(file)
                segmentation = sitk.GetArrayFromImage(segmentation_sitk)
                segmentation = np.moveaxis(segmentation, 0, 2)
                segmentation[segmentation == 2] = 0

                segmentation_sum = np.sum(segmentation, axis=2)
                segmentation_sum[segmentation_sum>0] = 1

                #print(np.argwhere(segmentation_sum==1))
                distances = []
                for element in (np.argwhere(segmentation_sum==1)):
                    distances.append(np.linalg.norm(element - [256,256]))

                max_distance = np.max(np.array(distances))
                # print(testSCase.id)
                # print(max_distance)
                # print(np.unique(segmentation[:,:,0]))
                # print(np.unique(segmentation[:,:,-1]))

                if len(np.unique(segmentation[:,:,0])) ==2 or len(np.unique(segmentation[:,:,-1])) ==2 or max_distance>254:
                    print("yes")
                else:
                    print("no")
