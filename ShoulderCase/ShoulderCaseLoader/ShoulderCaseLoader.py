import os
from getConfig import getConfig
from ShoulderCase.SCaseIDParser.SCaseIDParser import SCaseIDParser
from ShoulderCase.ShoulderCase.ShoulderCase import ShoulderCase
import glob
from utils.UpdatableText import UpdatableText
import pickle

class ShoulderCaseLoader:
    """
    Class to interact with the database.

    The main purpose of the ShoulderCaseLoader is to
    easily load the cases. By initialisation, a
    ShoulderCaseLoader instance will set its dataRootPath
    property to the dataDir variable found in the
    config.txt file.

    Because of cross-systems utilisation of the database
    the paths stored in the ShoulderCase instances may
    vary from one execution to the other.
    Thus, this class evolved to be the access point to
    the ShoulderCase constructor, to avoid misusing
    the paths. This was also the incentive to create the
    ShoulderCase.propagateDataPath() method.

    example:
    database = ShoulderCaseLoader()
    SCase = database.loadCase(['P500']);
    """
    def __init__(self):
        self.shoulderCasePath = {}
        self.dataRootPath = ""
        self.allCasesFound = False
        try:
            self.setDataRootPath(getConfig()["dataDir"])
        finally:
            pass

    def setDataRootPath(self, dataRootPath):
        assert os.path.isdir(dataRootPath), "%s is not a valid path."%dataRootPath

        # Do not reset obj.shoulderCasePath if dataRootPath doesn't change
        if self.dataRootPath == dataRootPath:
            return
        self.dataRootPath = dataRootPath
        self.shoulderCasePath = {}
        self.allCasesFound = False

    def containsCase(self, SCaseID):
        return self.findCase(SCaseID)

    def findCase(self, SCaseID):
        # Case already found
        if SCaseID in self.shoulderCasePath:
            return True

        # Format ID to construct the case's path
        givenID = SCaseIDParser(SCaseID)
        assert givenID.isValidID(), '%s is not a valid ID.' %SCaseID
        SCaseIDmaxDigits = givenID.getIDWithNumberOfDigits(getConfig()["maxSCaseIDDigits"])

        SCasePath = os.path.join(self.dataRootPath,
                                 SCaseIDmaxDigits[0], #SCaseID type
                                 SCaseIDmaxDigits[1], #SCaseID hundreds
                                 SCaseIDmaxDigits[2], #SCaseID tens
                                 SCaseID) + '*' #SCaseID folder whatever the IPP is

        SCaseFound = glob.glob(SCasePath)
        if not SCaseFound:
            return

        self.shoulderCasePath[SCaseID] = os.path.join(os.path.realpath(SCaseFound[0]))

        output = True
        return output

    def createEmptyCase(self, SCaseID):
        output = []

        # Recursion if a list of SCaseID is given
        if isinstance(SCaseID, list) and len(SCaseID) >= 1:
            for i in range(len(SCaseID)):
                output.append(self.createEmptyCase(SCaseID[i]))
            return output

        self.findCase(SCaseID)
        SCaseDataCTPath = ""

        SCaseCTFolders = glob.glob(os.path.join(self.shoulderCasePath[SCaseID], 'CT-*'))
        assert SCaseCTFolders, 'No CT folder found for %s' % SCaseID

        # Priority is given to folder containing a python archive
        for i in range(len(SCaseCTFolders)):
            if os.path.isdir(os.path.join(os.path.realpath(SCaseCTFolders[i]),
                                          getConfig()["landmarkAndSurfaceFilesFolder"],
                                          'SCase.pkl')):
                SCaseDataCTPath = os.path.join(os.path.realpath(SCaseCTFolders[i]))

        # Then, priority is given to folder containing a 'amira' folder
        if not SCaseDataCTPath:
            for i in range(len(SCaseCTFolders)):
                if os.path.isdir(os.path.join(os.path.realpath(SCaseCTFolders[i]), 'amira')):
                    SCaseDataCTPath = os.path.join(os.path.realpath(SCaseCTFolders[i]))

        # Then, priority is given to CT folder with lowest ending number
        if not SCaseDataCTPath:
            SCaseDataCTPath = os.path.join(os.path.realpath(SCaseCTFolders[0]))

        output = ShoulderCase(SCaseID, SCaseDataCTPath)
        return output

    def findAllCases(self):
        if self.allCasesFound:
            return

        types = getConfig()["SCaseIDValidTypes"]
        maxSCaseNumber = 10**(getConfig()["maxSCaseIDDigits"])-1

        progression = UpdatableText.UpdatableText('',' All valid cases are being looked for.')
        for i in range(len(types)):
            for j in range(1, maxSCaseNumber+1):
                progression.printPercent(getProgressionFraction(i,j,len(types),maxSCaseNumber))
                progression.printProgressBar(getProgressionFraction(i,j,len(types),maxSCaseNumber))
                SCaseID = types[i] + str(j)
                try:
                    self.findCase(SCaseID)
                except:
                    pass

        self.allCasesFound = True

    def getAllCasesID(self):
        self.findAllCases()
        casesID = list(self.shoulderCasePath.keys())
        return casesID

    def getAllNormalCasesID(self):
        self.findAllCases()
        casesID = list(self.shoulderCasePath.keys())
        for case in casesID:
            if case[0] == "P":
                casesID.remove(case)
        return casesID

    def getAllPathologicalCasesID(self):
        self.findAllCases()
        casesID = list(self.shoulderCasePath.keys())
        for case in casesID:
            if case[0] == "N":
                casesID.remove(case)
        return casesID

    def getCasePath(self, SCaseID):
        assert self.findCase(SCaseID), '%s not found in the database.' % SCaseID
        return self.shoulderCasePath(SCaseID)

    def getDataRootPath(self):
        return self.dataRootPath

    def getNumberOfFoundCases(self):
        return len(self.shoulderCasePath.keys())

    def loadAllCases(self):
        allCases = []

        self.findAllCases()
        allCasesID = list(self.shoulderCasePath.keys())
        totalNumberOfCases = self.getNumberOfFoundCases()
        progression = UpdatableText.UpdatableText('',' All cases are being loaded.')
        for i in range(totalNumberOfCases):
            progression.printPercent(i/totalNumberOfCases)
            progression.printProgressBar(i/totalNumberOfCases)
            try:
                allCases.append(self.loadCase(allCasesID[i]))
            finally:
                pass

        return allCases

    def loadCase(self, SCaseID):
        output = []

        # Recursion if a list of SCaseID is given
        if isinstance(SCaseID, list) and len(SCaseID) >= 1:
            for i in range(len(SCaseID)):
                output.append(self.loadCase(SCaseID[i]))
            return output

        assert self.findCase(SCaseID), '%s not found in the database.' % SCaseID

        # find SCase.pkl files
        SCaseFilePath = glob.glob(os.path.join(self.shoulderCasePath[SCaseID],
                                               'CT-*',
                                               getConfig()["landmarkAndSurfaceFilesFolder"],
                                               'SCase.pkl'))[0]
        assert SCaseFilePath, 'No archive found for %s' % SCaseID

        # Load a verified ShoulderCase instance
        filename = SCaseFilePath
        with open(filename, 'rb') as pkl_scase:
            try:
                loaded = pickle.load(pkl_scase)
            except:
                raise FileNotFoundError('No SCase field found in the archive')

        SCase = loaded
        assert isinstance(loaded, ShoulderCase), 'The found SCase is not a ShoulderCase instance'

        # Update data paths
        SCase.dataCTPath = os.sep.join(SCaseFilePath.split(os.sep)[:-2])

        return SCase

def getProgressionFraction(i,j,numberOfTypes,maxSCaseNumber):
    currentNumber = (i)*(maxSCaseNumber+1)+j
    maxNumber = numberOfTypes*(maxSCaseNumber+1)-1
    return currentNumber/maxNumber
