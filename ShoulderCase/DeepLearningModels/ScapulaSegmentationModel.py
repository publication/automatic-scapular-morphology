class ScapulaSegmentationModel:
    def __init__(self):
        """
        Here you can specify all of the deep learning model initialization variables such as
        the architecture of your networks.
        """
        pass

    def train(self):
        """
        This is where you train (fit) your network.
        :return:
        """
        pass

    def predict(self, testData):
        """
        Here you implement the prediction method of your model
        :return:
        """
        pass
