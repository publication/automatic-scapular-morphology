import json

import pydicom as dicom
import os
import numpy as np
import SimpleITK as sitk
import vtk
from scipy.ndimage import zoom
from skimage import measure
import pymeshlab
from getConfig import getConfig
from loadSCase import loadSCase


class SegmentationTraining:
    def __init__(self):
        """
        Please fill this method with all of the necessary initialization for segmenting scapula.
        We instantiate your deep learninf model here.
        You can have the list of the training cases here.
        """
        #self.scapulaSegmentorModel = model



    def createFolders(self):
        try:
            os.makedirs(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["segmentationTrainingParameters"]["segmTempPreprocessedFolder"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["segmentationTrainingParameters"]["modelSaveDir"])
        except OSError as error:
            print(error)

        try:
            os.makedirs(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_raw_data/" + "Task"+str(getConfig()["segmentationTrainingParameters"]["taskNumber"]) + "_" + getConfig()["segmentationTrainingParameters"]["taskName"] + "/imagesTr")
            os.makedirs(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_raw_data/" + "Task"+str(getConfig()["segmentationTrainingParameters"]["taskNumber"]) + "_" + getConfig()["segmentationTrainingParameters"]["taskName"] + "/imagesTs")
            os.makedirs(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_raw_data/" + "Task"+str(getConfig()["segmentationTrainingParameters"]["taskNumber"]) + "_" + getConfig()["segmentationTrainingParameters"]["taskName"] + "/labelsTr")
            os.mkdir(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_cropped_data")
        except OSError as error:
            print(error)

    def getImagesFromScaseID(self, trainSCase):

        print("Converting DICOMs to NIFTI and saving the images in training directory.")

        dicom_dir = trainSCase.dataDicomPath() + "/"
        slices = [dicom.dcmread(dicom_dir + q) for q in os.listdir(dicom_dir) if q.endswith(".dcm")]
        slices.sort(key=lambda x: float(x.ImagePositionPatient[2]))
        images = np.stack([s.pixel_array for s in slices]).astype(np.int32)
        images[images == images[0][0][0]] = 0
        images *= int(slices[0].RescaleSlope)
        images += int(slices[0].RescaleIntercept)
        params = [slices[0].PixelSpacing[0], slices[0].PixelSpacing[1],
                  abs(slices[0].ImagePositionPatient[2] - slices[1].ImagePositionPatient[2]),
                  slices[0].ImagePositionPatient[0], slices[0].ImagePositionPatient[1],
                  slices[0].ImagePositionPatient[2]]
        params = np.array(params).reshape((2, 3))
        sitkIm = sitk.GetImageFromArray(images)
        sitkIm.SetSpacing(params[0])
        sitkIm.SetOrigin(params[1])
        sitk.WriteImage(sitkIm, os.path.join(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_raw_data/" + "Task"+str(getConfig()["segmentationTrainingParameters"]["taskNumber"]) + "_" + getConfig()["segmentationTrainingParameters"]["taskName"] + "/imagesTr",
                                             (trainSCase.id + "_0000.nii.gz")))



    def getLabelsFromScaseID(self, trainSCase):
        python_dir = trainSCase.dataPythonPath() + "/"
        groundtruth_dir = python_dir + "groundTruth/segmentation/"
        sitkLabel = sitk.ReadImage(groundtruth_dir + trainSCase.id + ".nii.gz")
        sitk.WriteImage(sitkLabel, os.path.join(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_raw_data/" + "Task"+str(getConfig()["segmentationTrainingParameters"]["taskNumber"]) + "_" + getConfig()["segmentationTrainingParameters"]["taskName"] + "/labelsTr",
                                             (trainSCase.id + ".nii.gz")))

    def writeDataset(self, trainSCases):
        keysDataset = ['description', 'labels', 'licence', 'modality', 'name', 'numTest', 'numTraining', 'reference', 'release', 'tensorImageSize', 'test', 'training']
        dictDataset = dict.fromkeys(keysDataset)
        # python_dir = trainSCase.dataPythonPath() + "/"
        # groundtruth_dir = python_dir + "groundTruth/segmentation/"

        dictDataset["description"] = ""
        dictDataset["labels"] = {'0': 'background', '1': 'scapula', '2': 'humerus'}
        dictDataset["licence"] = "ARTORG/EPFL"
        dictDataset["modality"] = {"0": "CT"}
        dictDataset["name"] = "Shoulder"
        dictDataset["numTest"] = 0
        dictDataset["numTraining"] = len(trainSCases)
        dictDataset["reference"] = ""
        dictDataset["release"] = "1.0"
        dictDataset["tensorImageSize"] = "4D"
        dictDataset["test"] = []
        dictDataset["training"] = []
        keysTraining = ["image", "label"]
        SCases = loadSCase(trainSCases)
        for sCase in SCases:
            print(sCase.id)
            dictTraining = dict.fromkeys(keysTraining)
            dictTraining["image"] = "./imagesTr/" + sCase.id + ".nii.gz"
            dictTraining["label"] = "./labelsTr/" + sCase.id + ".nii.gz"
            dictDataset["training"].append(dictTraining)
            print(dictDataset["training"])

        with open(getConfig()["segmentationTrainingParameters"]["segmTempRawFolder"] + "/nnUNet_raw_data/" + "Task"+str(getConfig()["segmentationTrainingParameters"]["taskNumber"]) + "_" + getConfig()["segmentationTrainingParameters"]["taskName"] + "/dataset.json", 'w') as f:
            json.dump(dictDataset,f, indent = 4)

    def train(self):
        # image_folder = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/segmentations/"

        # os.system("nnUNet_predict -i %s -o %s -t %i -tr nnUNetTrainerOsman -m 2d -f %i --disable_tta" % (
        #     image_folder, image_folder, 500, 0))
        os.system("set OMP_NUM_THREADS=1")
        os.system("nnUNet_plan_and_preprocess -t %i --verify_dataset_integrity" % getConfig()["segmentationTrainingParameters"]["taskNumber"])
        os.system("nnUNet_train 3d_fullres nnUNetTrainerOsman %i all" % getConfig()["segmentationTrainingParameters"]["taskNumber"])


















