import pydicom as dicom
import os
import numpy as np
import SimpleITK as sitk
import vtk
from future.moves import sys
from scipy.ndimage import zoom
from skimage import measure
import pymeshlab
from getConfig import getConfig


class ScapulaSegmentation:
    def __init__(self, model):
        """
        Please fill this method with all of the necessary initialization for segmenting scapula.
        We instantiate your deep learninf model here.
        You can have the list of the training cases here.
        """
        self.scapulaSegmentorModel = model
        self.taskNumber = 506
        self.foldNumber = 5
        self.taskNumberCrop = 521
        self.foldNumberCrop = 0
        self.postprocessVoxelThreshold = 50000
        self.xViewInMm = 100 #Field of view for x/y/z coordinates on each side of the cropping point
        self.yViewInMm = 150
        self.zViewInMm = 150

    def largestConnectedLayer(self, image, background=0):
        labels = measure.label(image, background=background)
        counts = np.bincount(labels.flatten())
        argmax = np.argmax(counts[1:]) + 1
        return (labels == argmax).astype(int)

    def postprocess(self, image):
        single = self.largestConnectedLayer(image)

        return single

    def train(self, trainingSCases):
        """
        Here we train the deep learning model on training subjects
        :param trainingData:
        :return:
        """
        self.scapulaSegmentorModel.train(trainingSCases)

    def predictScapulaSegmentation(self, testSCase):
        """
        Here we call your deep learning model on one or many cases for prediction.
        :return:
        """
        try:
            os.mkdir(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/segmentations")
        except OSError as error:
            print("The directory exists. Delete the segmentation file if already segmented.")

        # Convert the DICOMS to nii.gz if they do not already exist
        if os.path.exists(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations",
                                           (testSCase.id + "_0000.nii.gz"))):
            print("DICOMs are already converted to NIFTI.")

        else:
            print("Converting DICOMs to NIFTI")

            dicom_dir = testSCase.dataDicomPath() + "/"
            slices = [dicom.dcmread(dicom_dir + q) for q in os.listdir(dicom_dir)] #if q.endswith(".dcm")
            slices.sort(key=lambda x: float(x.ImagePositionPatient[2]))
            images = np.stack([s.pixel_array for s in slices]).astype(np.int32)
            images[images == images[0][0][0]] = 0
            images += int(slices[0].RescaleIntercept)
            images *= int(slices[0].RescaleSlope)
            images[0][0][0]=3071
            params = []
            params.append(slices[0].PixelSpacing[0])
            params.append(slices[0].PixelSpacing[1])
            params.append(abs(slices[0].ImagePositionPatient[2] - slices[1].ImagePositionPatient[2]))
            params.append(slices[0].ImagePositionPatient[0])
            params.append(slices[0].ImagePositionPatient[1])
            params.append(slices[0].ImagePositionPatient[2])
            params = np.array(params).reshape((2, 3))

            spacing = params[0]
            origin = params[1]
            sitkIm = sitk.GetImageFromArray(images)
            sitkIm.SetSpacing(spacing)
            sitkIm.SetOrigin(origin)
            sitk.WriteImage(sitkIm, os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations",
                                                 (testSCase.id + "_0000.nii.gz")))

        ## Run the initial prediction of nnUNet to determine if it is a full body scan or not.



        image_folder = os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/segmentations/"

        print("Initial segmentation to determine if the scan consists of 1 or 2 shoulder.")
        os.system("set OMP_NUM_THREADS=1")
        os.system("nnUNet_predict -i %s -o %s -t %i -tr nnUNetTrainerModified -m 2d -f %i --disable_tta > nul 2>&1" % (image_folder, image_folder, self.taskNumberCrop, self.foldNumberCrop))
        ## Postprocess the initial segmentation to determine full body or not. This is done based on the number of connected component detected as scapula.
        ## The idea here is that after postprocessing (deleting every connected component that is predicted as scapula below 1000 voxel), the number of connected
        ## components are observed.
        initialSegm = sitk.ReadImage(os.path.join(image_folder, (testSCase.id + ".nii.gz")))
        segm_np = sitk.GetArrayFromImage(initialSegm)
        segm_np[segm_np == 2] = 0
        initialSegmScapula = sitk.GetImageFromArray(segm_np)
        initialSegmScapula.SetSpacing(initialSegm.GetSpacing())
        initialSegmScapula.SetOrigin(initialSegm.GetOrigin())
        initialSegmScapula.SetDirection(initialSegm.GetDirection())
        connectedSegm = sitk.ConnectedComponent(initialSegmScapula)
        sorted_component_image = sitk.RelabelComponent(connectedSegm, sortByObjectSize=True)
        sortedScapula = sitk.GetArrayFromImage(sorted_component_image)
        bincounts = list(np.bincount(sortedScapula.flatten()))
        if len(bincounts) < 3:
            bincounts.append(0)
        to_be_discarded = np.where(np.bincount(sortedScapula.flatten()) < bincounts[2])[0]
        for ind in to_be_discarded:
            sortedScapula[sortedScapula == ind] = 0
        postprocessed = sitk.GetImageFromArray(sortedScapula)
        postprocessed.SetSpacing(initialSegm.GetSpacing())
        postprocessed.SetOrigin(initialSegm.GetOrigin())
        postprocessed.SetDirection(initialSegm.GetDirection())
        stats = sitk.LabelShapeStatisticsImageFilter()
        stats.Execute(sitk.ConnectedComponent(postprocessed))
        left = []
        right = []
        for label in range(1, stats.GetNumberOfLabels() + 1):
            centroid = stats.GetCentroid(label)
            if centroid[0] < 0:
                right.append(centroid)
            else:
                left.append(centroid)

        size = initialSegm.GetSpacing()[0] * initialSegm.GetSpacing()[1] * initialSegm.GetSize()[0] * initialSegm.GetSize()[1]
        if right == [] or left == [] or bincounts[2] < 20000 or size < 85000:
        #if np.linalg.norm(left[0]-left[1]) < 300:
        ## If one of the shoulder does not exist, this means that this is not a full body scan.
            print("This CT contains only 1 shoulder. Proceeding with segmentation of the scapula.")
            os.remove(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/",
                                   (testSCase.id + ".nii.gz")))
            os.system("nnUNet_predict -i %s -o %s -t %i -tr nnUNetTrainerModified -m 3d_fullres -f %i --disable_tta > nul 2>&1" % (
            image_folder, image_folder, self.taskNumber, self.foldNumber))
            os.remove(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/", "plans.pkl"))

        ## If more than 2 connected component exists (background + 1 scapula), this means that this is a full body scan. The cropping is done based on each scapula's
        ## centroid. The assumption here is if the x coordinate is below 0, this is the right shoulder otherwise it is the left shoulder.
        ## From each centroid, the CT scan is cut 20x30x30cm (similar field of view to a regular shoulder CT), which later segmented separately.
        else:
            print("This CT contains 2 shoulders. Proceeding with cropping.")

            left = np.squeeze(left)
            right = np.squeeze(right)
            spacing = postprocessed.GetSpacing()
            origin = postprocessed.GetOrigin()
            x_voxel_right = int(
                (right[0] - origin[0]) / spacing[0])  # Finding the voxel that corresponds to the centroid locations.
            y_voxel_right = int((right[1] - origin[1]) / spacing[1])
            z_voxel_right = int((right[2] - origin[2]) / spacing[2])
            x_voxel_left = int((left[0] - origin[0]) / spacing[0])
            y_voxel_left = int((left[1] - origin[1]) / spacing[1])
            z_voxel_left = int((left[2] - origin[2]) / spacing[2])

            image2cut = sitk.ReadImage(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/",
                                                    (testSCase.id + "_0000.nii.gz")))

            left_image = image2cut[
                         np.maximum(0, int(x_voxel_left - (self.xViewInMm / spacing[1]))):int(x_voxel_left + (100 / spacing[1])),
                         np.maximum(0, int(y_voxel_left - (self.yViewInMm / spacing[0]))):int(y_voxel_left + (150 / spacing[0])),
                         np.maximum(0, int(z_voxel_left - (self.zViewInMm / spacing[2]))):int(z_voxel_left + (150 / spacing[2]))]

            right_image = image2cut[
                          np.maximum(0, int(x_voxel_right - (self.xViewInMm / spacing[1]))):int(
                              x_voxel_right + (self.xViewInMm / spacing[1])),
                          np.maximum(0, int(y_voxel_right - (self.yViewInMm / spacing[0]))):int(
                              y_voxel_right + (self.yViewInMm / spacing[0])),
                          np.maximum(0, int(z_voxel_right - (self.zViewInMm / spacing[2]))):int(
                              z_voxel_right + (self.zViewInMm / spacing[2]))]

            sitk.WriteImage(left_image, os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/",
                                                     (testSCase.id + "L_0000.nii.gz")))
            sitk.WriteImage(right_image,
                            os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/",
                                         (testSCase.id + "R_0000.nii.gz")))
            os.remove(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/",
                                   (testSCase.id + ".nii.gz")))  ##Deletion of some intermediate files
            os.remove(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/",
                                   (testSCase.id + "_0000.nii.gz")))
            os.remove(os.path.join(testSCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"] + "/segmentations/", "plans.pkl"))

            print("Cropping finished. Proceeding with segmentation of both shoulders")
            os.system("nnUNet_predict -i %s -o %s -t %i -tr nnUNetTrainerOsman -m 3d_fullres -f %i > nul 2>&1" % (
            image_folder, image_folder, self.taskNumber, self.foldNumber))




    def saveScapulaSegmentationVTK(self, testsCase):
        """
        Here we save the .ply file of scapula segmentation in the given filename
        :return:
        """
        image_folder = os.path.join(testsCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"]) + "/segmentations/"
        print("Segmentation/s have been completed. Converting the segmentations to surface meshes.")
        for file in os.listdir(image_folder): ## Here all the segmentations that has been saved are loaded. There would be 1 segmentation
                                              ## for a shoulder CT and 2 for full body.

            if file.endswith(".nii.gz") and not (file.endswith("_0000.nii.gz")):
                #The postprocessing selects the biggest component for scapula and humerus as the final segmentation.
                segm = sitk.ReadImage(os.path.join(image_folder, file))
                segm_np = sitk.GetArrayFromImage(segm)
                segm_np[segm_np == 2] = 0
                labels = measure.label(segm_np, background=0)
                counts = np.bincount(labels.flatten())
                argmax = np.argmax(counts[1:]) + 1
                segm_np_pp_scapula = (labels == argmax).astype(int)
                #segm = sitk.ReadImage(os.path.join(image_folder, file))
                segm_np = sitk.GetArrayFromImage(segm)
                segm_np[segm_np == 1] = 0
                labels = measure.label(segm_np, background=0)
                counts = np.bincount(labels.flatten())
                argmax = np.argmax(counts[1:]) + 1
                segm_np_pp_humerus = (labels == argmax).astype(int) * 2
                segm_np_combined = segm_np_pp_scapula + segm_np_pp_humerus

                segm_postprocessed = sitk.GetImageFromArray(segm_np_combined)
                segm_postprocessed.SetSpacing(segm.GetSpacing())
                segm_postprocessed.SetOrigin(segm.GetOrigin())
                segm_postprocessed.SetDirection(segm.GetDirection())
                sitk.WriteImage(segm_postprocessed, os.path.join(image_folder, file)) ## The output of nnUNet is replaced with postprocessed segmentation.

                dataReader = vtk.vtkNIFTIImageReader()
                dataReader.SetFileName(os.path.join(image_folder, file))
                dataReader.Update()
                origin = sitk.ReadImage(os.path.join(image_folder, file)).GetOrigin()
                resolution = sitk.ReadImage(os.path.join(image_folder, file)).GetSize()

                ##The segmentation is moved to its original origin. This is done because vtk ignores the actual origin and sets it to (0,0,0)
                vtkSegmentationScapula = dataReader.GetOutput()
                vtkSegmentationScapula.SetOrigin(origin)


                ##Here the segmentation is thresholded for meshing only the scapula. The same is later done for humerus, such that two bones have separate meshes.
                thresholdScapula = vtk.vtkImageThreshold()
                thresholdScapula.SetInputData(vtkSegmentationScapula) ##Note that in the segmentation file, scapula label is 1 and humerus label is 2.
                thresholdScapula.ThresholdBetween(0.5, 1.5)
                thresholdScapula.ReplaceOutOn()
                thresholdScapula.SetOutValue(0)
                thresholdScapula.Update()

                ##The paddding is done because if the first and/or last slice contains segmentation, then the resulting mesh is not closed surface. This is due to a behavior of vtk, and the workaround is padding a voxel in each dimension.
                padScapula = vtk.vtkImageConstantPad()
                padScapula.SetInputConnection(thresholdScapula.GetOutputPort())
                padScapula.SetOutputWholeExtent(-1, resolution[0] + 1,
                                         -1, resolution[1]+ 1,
                                         -1, resolution[2] + 1)
                padScapula.SetConstant(0)
                padScapula.Update()
                vtkScapula = padScapula.GetOutput()

                vtkSegmentationHumerus = dataReader.GetOutput()
                vtkSegmentationHumerus.SetOrigin(origin)
                thresholdHumerus = vtk.vtkImageThreshold()
                thresholdHumerus.SetInputData(vtkSegmentationHumerus)
                thresholdHumerus.ThresholdBetween(1.5, 2.5)
                thresholdHumerus.ReplaceOutOn()
                thresholdHumerus.SetOutValue(0)
                thresholdHumerus.Update()
                padHumerus = vtk.vtkImageConstantPad()
                padHumerus.SetInputConnection(thresholdHumerus.GetOutputPort())
                padHumerus.SetOutputWholeExtent(-1, resolution[0] + 1,
                                         -1, resolution[1] + 1,
                                         -1, resolution[2] + 1)
                padHumerus.SetConstant(0)
                padHumerus.Update()
                vtkHumerus = padHumerus.GetOutput()

                ##Flying edges algorithm is used in Slicer for meshing.
                contactsScapula = vtk.vtkFlyingEdges3D()
                contactsScapula.SetInputData(vtkScapula)
                contactsScapula.SetValue(0, 1)
                contactsScapula.ComputeNormalsOn()
                contactsScapula.ComputeGradientsOn()
                contactsScapula.ComputeScalarsOn()
                contactsScapula.Update()
                vtkPolyDataScapula = contactsScapula.GetOutput()

                contactsHumerus = vtk.vtkFlyingEdges3D()
                contactsHumerus.SetInputData(vtkHumerus)
                contactsHumerus.SetValue(1, 2)
                contactsHumerus.ComputeNormalsOn()
                contactsHumerus.ComputeGradientsOn()
                contactsHumerus.ComputeScalarsOn()
                contactsHumerus.Update()
                vtkPolyDataHumerus = contactsHumerus.GetOutput()

                ##Center of mass calculation for scapula and humerus.
                centerOfMassFilter = vtk.vtkCenterOfMass()
                centerOfMassFilter.SetInputData(vtkPolyDataScapula)
                centerOfMassFilter.Update()
                centerOfScapula = centerOfMassFilter.GetCenter()
                centerOfMassFilter.SetInputData(vtkPolyDataHumerus)
                centerOfMassFilter.Update()
                centerOfHumerus = centerOfMassFilter.GetCenter()

                passBand = pow(10.0, -4.0 * 0.6)
                ##Bandpass filter for smoothing.
                smootherScapula = vtk.vtkWindowedSincPolyDataFilter()
                smootherScapula.SetInputData(vtkPolyDataScapula)
                smootherScapula.SetPassBand(passBand)
                smootherScapula.BoundarySmoothingOff()
                smootherScapula.FeatureEdgeSmoothingOff()
                smootherScapula.NonManifoldSmoothingOn()
                smootherScapula.NormalizeCoordinatesOn()
                smootherScapula.SetNumberOfIterations(20)
                smootherScapula.Update()

                smootherHumerus = vtk.vtkWindowedSincPolyDataFilter()
                smootherHumerus.SetInputData(vtkPolyDataHumerus)
                smootherHumerus.SetPassBand(passBand)
                smootherHumerus.BoundarySmoothingOff()
                smootherHumerus.FeatureEdgeSmoothingOff()
                smootherHumerus.NonManifoldSmoothingOn()
                smootherHumerus.NormalizeCoordinatesOn()
                smootherHumerus.SetNumberOfIterations(20)
                smootherHumerus.Update()

                writerPLY = vtk.vtkPLYWriter()
                
                if centerOfHumerus[0] < centerOfScapula[0]:
                # if 1>0:
                    #If the humerus is more on the left than scapula, it is a left shoulder. Otherwise it is a right shoulder.
                    writerPLY.SetFileName(os.path.join(testsCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"], ("scapulaSurfaceAutoR.ply")))
                    writerPLY.SetInputConnection(smootherScapula.GetOutputPort())
                    writerPLY.SetFileTypeToBinary()
                    writerPLY.Write()
                    writerPLY.SetFileName(
                        os.path.join(testsCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"],
                                     ("humerusSurfaceAutoR.ply")))
                    writerPLY.SetInputConnection(smootherHumerus.GetOutputPort())
                    writerPLY.SetFileTypeToBinary()
                    writerPLY.Write()

                else:
                    writerPLY.SetFileName(
                        os.path.join(testsCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"],
                                     ("scapulaSurfaceAutoL.ply")))
                    writerPLY.SetInputConnection(smootherScapula.GetOutputPort())
                    writerPLY.SetFileTypeToBinary()
                    writerPLY.Write()
                    writerPLY.SetFileName(
                        os.path.join(testsCase.dataCTPath, getConfig()["landmarkAndSurfaceFilesFolder"],
                                     ("humerusSurfaceAutoL.ply")))
                    writerPLY.SetInputConnection(smootherHumerus.GetOutputPort())
                    writerPLY.SetFileTypeToBinary()
                    writerPLY.Write()





