# AUTOMATIC QUANTIFICATION OF SCAPULAR AND GLENOID MORPHOLOGY FROM CT SCANS USING DEEP LEARNING

## Introduction

This repository contains the source code for the implementation explained in the paper "AUTOMATIC QUANTIFICATION OF 
SCAPULAR AND GLENOID MORPHOLOGY FROM CT SCANS USING DEEP LEARNING". 


## Setup

### Installation
* Clone or download the repository.

* Install the requirements.txt (pip3 install -r requirements.txt)

* Install pytorch by following the guidelines in pytorch.org (Any version later than 1.10.1 should work. The code was tested on 1.10.1 with CUDA version 11.1)

* Depending on your OS, there are two possibilities for the segmentation module (this code uses nnUNet v1, https://github.com/MIC-DKFZ/nnUNet):


* Linux:

    * Keep the original installation of nnUNet in the virtual environment.

    * In the ScapulaSegmentation.py file, there are some commands running with "os.system" function. The commands are Windows commands by default, these should be changed
to their Linux equivalent.

    * You also need to set environment variables for nnUNet to work. Please follow the instructions from https://github.com/MIC-DKFZ/nnUNet. The enviroment variable for the models
should be where you locate the pretrained model downloaded from our repo.

* Windows:

    * Originally, the nnUNet repository in this link supports only Linux and is installed within requirements.txt. 
We used the repository in https://github.com/marcus-wirtz-snkeos/nnUNet-Windows for Windows support by replacing the files in
virtual environment with the files in this repository. (If the virtual environment is named venv, the folder venv\Lib\site-packages\nnunet\ should be replaced
with this repository.)

    * The idea of setting environment variables is similar to Linux, but it's done differently in Windows. 
 
    * Using the windows GUI:

        * Open the start menu.
        * Search for the "Advanced System Settings" control panel and click on it.
        * Click on the "Environment Variables" button toward the bottom of the screen.
        * Follow the prompts to add the variable to the user table.
      
    * Also you need to install batchgenerators with: pip3 install batchgenerators==0.21.0
    

* Copy the nnUNetTrainerModified.py file to the location where nnUNet is installed (if the virtual environment is named venv, the location would be
\venv\Lib\site-packages\nnunet\training\network_training)

* Run setup.py to create a config.json file.

    * This setup() function will create a config.json file. The steps to adapt this setup.py function to test your own dataset
is described below.

### Write the config file

The base folder must contain a "config.json" with the following keys:


* dataDir: This is the root path to the data used. By default the dataDir is initialized as "Data/" (As Data folder is in the directory of the code).
This means that the directory of the dicoms that you would like to measure must be "Data/P/1/0/P100/CT-P100-1/dicom/". 
The code would read the dicoms in this directory and would measure the parameters.

* landmarkAndSurfaceFilesFolder: This is the folder where the output of the results would be written (Segmentation, landmarks, parameters)

* scapulaLandmarkModel (and glenoidLandmarkModel): The locations for the landmark models.

* Feel free to play with the other parameters, but these keys in config.json is absolutely necessary in order to run the code with your own data.

## How to use

* After the installation and setting up the dataDir correctly, the user can run test.py file to do the measurements.

* There are 5 functions running the test.py file. By running order:
    * measureScase(["P100"]) --> Instantiates an empty measurement file.
    * segmentSCases(["P100"]) --> Checks if the scan contains 1 or 2 shoulder, segments the scapula (and humerus), saves them as NIfTI and .ply files.
    * predictLandmarksSCases(["P100"]) --> Predicts the landmarks for scapula and glenoid, and extracts the glenoid surface.
    * measureScase(["P100"]) --> Measures and saves the anatomical parameters based on the segmentation, landmarks, and glenoid surface.
    * case = loadSCase("P100") --> Reads the measurement file.

* Then, the 6 anatomical parameters (version, inclination, CSA, GPA, glenoid width and height) is printed for both shoulders (should take around 10 minutes for one shoulder). If the scan contains only 1 shoulder,
the code will print the values for the corresponding side, and will return empty arrays for the one that is not in field of view.

*The models in this project and an example CT can be found in this link: https://zenodo.org/records/11472053. The NIFTI file can be put in Data/P/1/9/P194/CT-P194-1/result/segmentations folder to bypass DICOM reader and test this NIFTI.