import json

def setup():
    # to execute once after cloning the repo.
    createDefaultConfigFile()
    
def createDefaultConfigFile():
    defaultConfig = {}
    defaultConfig["maxSCaseIDDigits"] = 3
    defaultConfig["SCaseIDValidTypes"] = ["N", "P"]
    defaultConfig["dataDir"] = "Data/"
    defaultConfig["landmarkAndSurfaceFilesFolder"] = "result"
    defaultConfig["casesToMeasure"] = ["P100"]
    defaultConfig["scapulaLandmarkModel"] = "Models/scapulaLandmarkModel/"
    defaultConfig["glenoidLandmarkModel"] = "Models/glenoidLandmarkModel/"


    defaultConfig["shouldersToMeasure"] = {}
    defaultConfig["shouldersToMeasure"]["rightAuto"] = True
    defaultConfig["shouldersToMeasure"]["rightManual"] = True
    defaultConfig["shouldersToMeasure"]["leftAuto"] = True
    defaultConfig["shouldersToMeasure"]["leftManual"] = True
    
    defaultConfig["runMeasurements"] = {}
    defaultConfig["runMeasurements"]["loadData"] = True
    defaultConfig["runMeasurements"]["sliceAndSegmentRotatorCuffMuscles"] = False
    defaultConfig["runMeasurements"]["morphology"] = True
    defaultConfig["runMeasurements"]["measureFirst"] = True
    defaultConfig["runMeasurements"]["measureSecond"] = True
    defaultConfig["runMeasurements"]["measureGlenoidDensity"] = False

    defaultConfig["rotatorCuffSliceName"] = ""
    defaultConfig["rotatorCuffSegmentationName"] = ""
    defaultConfig["muscleSubdivisionsResolutionInMm"] = {}
    defaultConfig["muscleSubdivisionsResolutionInMm"]["x"] = 5
    defaultConfig["muscleSubdivisionsResolutionInMm"]["y"] = 5

    defaultConfig["overwriteMeasurements"] = True
    defaultConfig["saveMeasurements"] = True
    defaultConfig["saveAllMeasurementsInOneFile"] = False
    
    with open("config.json", "w") as json_file:
        json.dump(defaultConfig, json_file, indent=1)
        
        
    

    
    