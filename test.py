from setup import setup
from measureSCase import measureScase
from plotSCases import plotSCases
import pickle
import numpy as np
import pandas as pd
from loadSCase import loadSCase
from segmentSCases import segmentSCases, trainDLSegmentationModel, trainScapulaLandmarkModel
from segmentSCases import predictLandmarksSCases
import os

# run setup
setup()

# do the measurements
measureScase(["P100"])
segmentSCases(["P100"])
predictLandmarksSCases(["P100"])
measureScase(["P100"])
case = loadSCase("P100")



print("Left shoulder:")
print(f"GVA:{np.round(case.shoulders['left']['auto'].scapula.glenoid.version,2)}°")
print(f"GIA:{np.round(case.shoulders['left']['auto'].scapula.glenoid.inclination,2)}°")
print(f"CSA:{np.round(case.shoulders['left']['auto'].scapula.acromion.CSA,2)}°")
print(f"GPA:{np.round(case.shoulders['left']['auto'].scapula.glenoid.GPA,2)}°")
print("Glenoid Width: " + str(np.round(case.shoulders['left']['auto'].scapula.glenoid.width, 2)) + " mm")
print("Glenoid Height: " + str(np.round(case.shoulders['left']['auto'].scapula.glenoid.height, 2)) + " mm")
print("\n")
print("Right shoulder:")
print(f"GVA: {np.round(case.shoulders['right']['auto'].scapula.glenoid.version,2)}°")
print(f"GIA: {np.round(case.shoulders['right']['auto'].scapula.glenoid.inclination,2)}°")
print(f"CSA: {np.round(case.shoulders['right']['auto'].scapula.acromion.CSA,2)}°")
print(f"GPA: {np.round(case.shoulders['right']['auto'].scapula.glenoid.GPA,2)}°")
print("Glenoid Width: " + str(np.round(case.shoulders['right']['auto'].scapula.glenoid.width, 2)) + " mm")
print("Glenoid Height: " + str(np.round(case.shoulders['right']['auto'].scapula.glenoid.height, 2)) + " mm")

